Drupal.behaviors.hidey_help = function (d) {
  var hh = $("#content-header div.help");
  if (hh.length) {
    hh.before("<a class='help-hider' style='display:block;border:1px solid black;padding:2px 4px;'><span class='hide'>Hide</span><span class='show'>Show</span> Help</a>");
    $("a.help-hider .hide").hide();
    hh.hide();
    $("a.help-hider").click(function() {
      if ($("#content-header div.help:visible").length) {
        $("#content-header div.help").hide('fast');
        $("a.help-hider .hide").hide();
        $("a.help-hider .show").show();
      }
      else {
        $("#content-header div.help").show('fast');
        $("a.help-hider .hide").show();
        $("a.help-hider .show").hide();
      }
      return true;
    });
  }
}
